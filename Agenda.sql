create database  agendaF222;
use agendaF222;
create table tipo_usuarios(
id int not null primary key auto_increment,
nombre varchar(45)
)engine InnoDB;
create table personas(
id int not null primary key auto_increment,
nombre varchar(45),
apellido varchar(45),
telefono varchar(15),
correo text
)engine InnoDB;
create table usuarios(
id int not null primary key auto_increment,
nombre varchar(45),
pass blob,
persona int,
foreign key(persona) references personas(id),
tipo int,
foreign key(tipo) references tipo_usuarios(id),
estado boolean
)engine InnoDB;
create table t_actividades(
id int not null primary key auto_increment,
nombre varchar(45)
)engine InnoDB;
create table actividades(
id int not null primary key auto_increment,
nombre varchar(45),
descripcion text,
fecha date,
tipo int,
foreign key(tipo) references t_actividades(id),
usuario int,
foreign key(usuario) references usuarios(id),
persona int,
foreign key(persona) references personas(id)
)engine InnoDB;
create table faces(
id int not null primary key auto_increment,
nombre varchar(45)
)engine InnoDB;
create table estados(
id int not null primary key auto_increment,
nombre varchar(45)
)engine InnoDB;
-- actividad_fa se refiere a las faces de la actividad
create table actividad_fa(
id int not null primary key auto_increment,
fecha date,
face int,
foreign key(face) references faces(id),
estado int,
foreign key(estado) references estados(id),
actividad int,
foreign key(actividad) references actividades(id)
)engine InnoDB;

create table fecha_especiales(
id int not null primary key auto_increment,
fecha date not null,
estado boolean,
descripcion text
)engine InnoDB;
