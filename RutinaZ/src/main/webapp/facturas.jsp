<%-- 
    Document   : facturas
    Created on : 01-03-2020, 02:16:01 PM
    Author     : fredy.romerousam
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="${pageContext.request.servletContext.contextPath}/assets/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Facturas Relacionadas</h1>
        <div class="container">
            <div class="row">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Factura</th>
                            <th>FECHA</th>
                            <th>MODO de PAGO</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${lista}" var="lis">
                            <tr>
                                <td>${lis.numFactura}</td>
                                <td>${lis.fecha}</td>
                                <td>${lis.modoPago.nombre}</td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>

        </div>
    </body>
</html>
