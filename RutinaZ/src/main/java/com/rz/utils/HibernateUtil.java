/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rz.utils;

import com.rz.models.*;
import java.util.Properties;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.service.ServiceRegistry;

/**
 * Hibernate Utility class with a convenient method to get Session Factory
 * object.
 *
 * @author fredy.romerousam
 */
public class HibernateUtil {

    private static SessionFactory sessionFactory;

    public static SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            try {
                Configuration c = new Configuration();
                Properties p = new Properties();
                p.put(Environment.DRIVER, "com.mysql.jdbc.Driver");
                p.put(Environment.URL, "jdbc:mysql://localhost:3306/rutinaz?useSSL=false");
                p.put(Environment.USER, "root");
                p.put(Environment.PASS, "root");
                p.put(Environment.DIALECT, "org.hibernate.dialect.MySQLDialect");
                p.put(Environment.SHOW_SQL, "true");
                p.put(Environment.CURRENT_SESSION_CONTEXT_CLASS, "thread");
                c.setProperties(p);
                c.addAnnotatedClass(Cliente.class);
                c.addAnnotatedClass(Categoria.class);
                c.addAnnotatedClass(Detalle.class);
                c.addAnnotatedClass(Factura.class);
                c.addAnnotatedClass(ModoPago.class);
                c.addAnnotatedClass(Producto.class);

                /*clases
             
                 */
                ServiceRegistry sr = new StandardServiceRegistryBuilder().applySettings(c.getProperties()).build();
                sessionFactory = c.buildSessionFactory(sr);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return sessionFactory;
    }
}
