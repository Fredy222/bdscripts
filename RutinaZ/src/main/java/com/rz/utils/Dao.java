/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rz.utils;

import java.util.List;

/**
 *
 * @author fredy.romerousam
 */
public interface Dao<T> {

    public void save(T t);

    public T readById(int id);

    public void update(T t);

    public void delete(T t);

    List<T> findAll();
}
