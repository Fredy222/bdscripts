/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rz.utils;

import com.rz.dao.ClientesImp;
import com.rz.dao.FacturaImp;
import com.rz.dao.ProductoImp;
import com.rz.models.Cliente;
import com.rz.models.Producto;
import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

/**
 *
 * @author fredy.romerousam
 */
@Configuration
@EnableWebMvc
@ComponentScan("com.rz")
@EnableTransactionManagement(proxyTargetClass = true)
public class Conf {

    @Bean
    InternalResourceViewResolver viewRes() {
        InternalResourceViewResolver r = new InternalResourceViewResolver();
        r.setPrefix("/");
        r.setSuffix(".jsp");
        return r;
    }

    @Bean
    public SessionFactory getConex() {
        return HibernateUtil.getSessionFactory();
    }

    @Bean
    public Dao<Cliente> cDao() {
        return new ClientesImp();
    }

    @Bean
    public FacturaImp fDao() {
        return new FacturaImp();
    }
    
    @Bean
    public Dao<Producto> pDao() {
        return new ProductoImp();
    }
}
