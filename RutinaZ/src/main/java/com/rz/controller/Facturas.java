/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rz.controller;

import com.rz.dao.FacturaImp;
import com.rz.models.Factura;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.GET;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author fredy.romerousam
 */
@Controller
public class Facturas {

    ModelAndView mv = new ModelAndView();

     @Autowired
    private FacturaImp fDao;
    
    @RequestMapping(value = "/f",method = RequestMethod.GET)
    public ModelAndView factura(@RequestParam("c") int d) {
        List<Factura> lista=new ArrayList<>();
        lista=fDao.listar(d);
        mv.addObject("lista",lista);
        mv.setViewName("facturas");
        return mv;
    }
}
