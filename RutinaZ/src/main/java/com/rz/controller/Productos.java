/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rz.controller;

import com.rz.dao.ProductoImp;
import com.rz.models.Producto;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author fredy.romerousam
 */
@Controller
public class Productos {

    ModelAndView mv = new ModelAndView();

    @Autowired
    private ProductoImp pDao;

    @RequestMapping("/ver")
    public ModelAndView ver() {
        pDao=new ProductoImp();
        List<Producto> list=new ArrayList<>();
        list=pDao.findAll();
        mv.addObject("lis",list);
        mv.setViewName("pro");
        return mv;
    }
}
