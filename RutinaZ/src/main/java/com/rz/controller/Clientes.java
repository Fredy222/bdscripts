/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rz.controller;

import com.rz.dao.ClientesImp;
import com.rz.models.Cliente;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author fredy.romerousam
 */
@SuppressWarnings("unused")
@Controller
public class Clientes {

    ModelAndView mv = new ModelAndView();

    @Autowired
    private ClientesImp cDao;

    @RequestMapping(value = "/persona")
    public ModelAndView carP() {
        cDao = new ClientesImp();
        List<Cliente> lc = new ArrayList<>();
        lc = cDao.findAll();
        System.out.println("Informacionde llegada -----");
        System.out.println("tamaño"+lc.size());
       mv.addObject("lis", lc);
        mv.setViewName("persona");
        return mv;
    }
}
