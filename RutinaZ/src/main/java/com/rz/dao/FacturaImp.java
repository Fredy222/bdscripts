/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rz.dao;

import com.rz.models.Factura;
import com.rz.utils.AbstractFacade;
import com.rz.utils.Dao;
import com.rz.utils.HibernateUtil;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Session;

/**
 *
 * @author fredy.romerousam
 */
public class FacturaImp extends AbstractFacade<Factura> implements Dao<Factura> {

    Session session = HibernateUtil.getSessionFactory().openSession();

    public FacturaImp() {
        super(Factura.class);
    }

    public List<Factura> listar(int d) {
        List<Factura> lista = new ArrayList<>();
        try {
            session.beginTransaction();
            lista = session.createNativeQuery("Select * From factura where id_cliente=" + d, Factura.class).getResultList();
            session.getTransaction().commit();
            return lista;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
