/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rz.dao;
import com.rz.models.Cliente;
import com.rz.utils.AbstractFacade;
import com.rz.utils.Dao;
import org.springframework.stereotype.Repository;

/**
 *
 * @author fredy.romerousam
 */
@Repository
public class ClientesImp extends AbstractFacade<Cliente> implements Dao<Cliente> {

    public ClientesImp() {
        super(Cliente.class);
    }

}
