<%-- 
    Document   : index
    Created on : 01-03-2020, 10:51:38 AM
    Author     : fredy.romerousam
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <link href="${pageContext.request.servletContext.contextPath}/assets/css/bootstrap.css"
              rel="stylesheet">
        <title>JSP Page</title>

    </head>
    <body>
        <div class="container">
            <h1>Rutina Z</h1>
            <div class="row">
                <a class="btn btn-danger" href="persona">Cliente</a>
                <a class="btn btn-success" href="ver">Producto</a>
            </div>

        </div>
    </body>
</html>
