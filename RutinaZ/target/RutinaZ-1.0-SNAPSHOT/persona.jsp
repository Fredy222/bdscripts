<%-- 
    Document   : persona
    Created on : 01-03-2020, 11:25:42 AM
    Author     : fredy.romerousam
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="${pageContext.request.servletContext.contextPath}/assets/css/bootstrap.css"
              rel="stylesheet">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Listado de Clientes</h1>
        <table class="table table-bordered" >
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Telefono</th>
                    <th>Email</th>
                    <th>Opcion</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${lis}" var="l">
                    <tr>
                        <td>${l.nombre} ${l.apellido}</td>
                        <td>${l.telefono}</td>
                        <td>${l.email}</td>
                        <td><a href="f?c=${l.idCliente}" class="btn btn-info">Ver Facturas</a></td>
                    </tr> 
                </c:forEach>
            </tbody>
        </table>
    </body>
</html>
