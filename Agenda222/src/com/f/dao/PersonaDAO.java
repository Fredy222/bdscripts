package com.f.dao;

import java.util.List;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.f.models.Personas;
import com.f.utils.AbstractFacade;
import com.f.utils.Dao;
import com.f.utils.HibernateUtil;

@Repository
public class PersonaDAO extends AbstractFacade<Personas> implements Dao<Personas> {

    private Session session = HibernateUtil.getSessionFactory().openSession();

    public PersonaDAO() {
        super(Personas.class);
    }

    public List<Personas> listaP() {
        String c = "SELECT p.* FROM personas as p inner join usuarios as u on u.persona=p.id where u.tipo=3";
        try {
            session.beginTransaction();
            List<Personas> per = session.createNativeQuery(
                    c, Personas.class).getResultList();
            session.flush();
            session.getTransaction().commit();;
            return per;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
