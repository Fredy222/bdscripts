package com.f.dao;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.f.models.TActividades;
import com.f.utils.AbstractFacade;
import com.f.utils.Dao;
import com.f.utils.HibernateUtil;

@Repository
public class TActividadesDAO extends AbstractFacade<TActividades> implements Dao<TActividades>{
	private Session session = HibernateUtil.getSessionFactory().openSession();

	public TActividadesDAO() {
		super(TActividades.class);
	}
}
