package com.f.dao;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import com.f.models.Actividades;
import com.f.reporte.Rep01;
import com.f.utils.AbstractFacade;
import com.f.utils.Dao;
import com.f.utils.HibernateUtil;

import java.util.ArrayList;
import java.util.List;

@Repository
public class ActividaDAO extends AbstractFacade<Actividades> implements Dao<Actividades> {

	private Session session = HibernateUtil.getSessionFactory().openSession();

	public ActividaDAO() {
		super(Actividades.class);
	}

	public List<Actividades> verificacion(int f) {
		String c = "SELECT * FROM actividades where persona =" + f + ";";
		try {
			session.beginTransaction();
			List<Actividades> fech = session.createNativeQuery(c, Actividades.class).getResultList();
			session.flush();
			session.getTransaction();
			return fech;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public List<Actividades> cActividad(int d) {
		System.out.println("Error en otro lado");
		String c = "SELECT * FROM actividades where usuario =" + d + ";";
		try {
			session.beginTransaction();
			List<Actividades> fech = session.createNativeQuery(c, Actividades.class).getResultList();
			session.flush();
			session.getTransaction().commit();
			return fech;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public List<Rep01> cargarA(int id) {
		List<Actividades> ca = new ArrayList<>();
		ca = cActividad(id);
		System.out.println("Error esta mas abajo " + ca.size());
		List<Rep01> lista = new ArrayList<>();
		try {
			for (int i = 0; i < ca.size(); i++) {
				String c = "call calculo('" + id + "','" + ca.get(i).getId() + "')";
				Transaction t = session.beginTransaction();
				SQLQuery query = session.createSQLQuery(c);
				List<Object[]> rows = query.list();
				t.commit();
				// lista
				Rep01 r = new Rep01();
				Object[] obj = rows.get(0);
				r.setNombre(obj[0].toString());
				r.setDias(obj[1].toString());
				r.setPersona(obj[2].toString());
				lista.add(r);
			}
			return lista;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}

	}

}
