package com.f.dao;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.f.models.Estados;
import com.f.utils.AbstractFacade;
import com.f.utils.Dao;
import com.f.utils.HibernateUtil;

@Repository
public class EstadoDAO extends AbstractFacade<Estados> implements Dao<Estados>{
	private Session session = HibernateUtil.getSessionFactory().openSession();

	public EstadoDAO() {
		super(Estados.class);
	}
}
