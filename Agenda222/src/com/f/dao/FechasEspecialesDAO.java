package com.f.dao;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.f.models.Faces;
import com.f.models.FechaEspeciales;
import com.f.models.Personas;
import com.f.utils.AbstractFacade;
import com.f.utils.Dao;
import com.f.utils.HibernateUtil;
import java.util.List;

@Repository
public class FechasEspecialesDAO extends AbstractFacade<FechaEspeciales> implements Dao<FechaEspeciales> {

    private Session session = HibernateUtil.getSessionFactory().openSession();

    public FechasEspecialesDAO() {
        super(FechaEspeciales.class);
    }

    public boolean verificacion(String f) {
        String c = "select * from fecha_especiales where fecha='"+f+"'and estado=true";
        try {
            session.beginTransaction();
            List<FechaEspeciales> fech = session.createNativeQuery(
                    c, FechaEspeciales.class).getResultList();
            session.flush();
            session.getTransaction().commit();
            System.out.println("tama�o de la lista "+fech.size());
            if (fech.size() > 0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return true;
        }
    }
}
