package com.f.dao;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.f.models.TipoUsuarios;
import com.f.utils.AbstractFacade;
import com.f.utils.Dao;
import com.f.utils.HibernateUtil;

@Repository
public class TipoUsuarioDAO extends AbstractFacade<TipoUsuarios> implements Dao<TipoUsuarios>{
	private Session session = HibernateUtil.getSessionFactory().openSession();

	public TipoUsuarioDAO() {
		super(TipoUsuarios.class);
	}
}
