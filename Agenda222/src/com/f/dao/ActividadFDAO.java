package com.f.dao;

import org.hibernate.Session;

import org.springframework.stereotype.Repository;

import com.f.models.ActividadFa;
import com.f.models.Actividades;
import com.f.utils.AbstractFacade;
import com.f.utils.Dao;
import com.f.utils.HibernateUtil;

@Repository
public class ActividadFDAO extends AbstractFacade<ActividadFa> implements Dao<ActividadFa> {
	private Session session = HibernateUtil.getSessionFactory().openSession();
	public ActividadFDAO() {
		super(ActividadFa.class);
	}
}