package com.f.dao;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.f.models.Faces;
import com.f.utils.AbstractFacade;
import com.f.utils.Dao;
import com.f.utils.HibernateUtil;

@Repository
public class FacesDAO extends AbstractFacade<Faces> implements Dao<Faces>{
	private Session session = HibernateUtil.getSessionFactory().openSession();

	public FacesDAO() {
		super(Faces.class);
	}
}
