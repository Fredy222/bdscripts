package com.f.utils;

import java.util.Properties;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.service.ServiceRegistry;

import com.f.models.ActividadFa;
import com.f.models.Actividades;
import com.f.models.Estados;
import com.f.models.Faces;
import com.f.models.FechaEspeciales;
import com.f.models.Personas;
import com.f.models.TActividades;
import com.f.models.TipoUsuarios;
import com.f.models.Usuarios;

public class HibernateUtil {
	private static SessionFactory sessionFactory;

	public static SessionFactory getSessionFactory() {
		if (sessionFactory == null) {
			try {
				Configuration configuration = new Configuration();
				// Hibernate settings equivalent to hibernate.cfg.xml's properties
				Properties settings = new Properties();
				settings.put(Environment.DRIVER, "com.mysql.jdbc.Driver");
				settings.put(Environment.URL, "jdbc:mysql://usam-sql.sv.cds:3306/agendaf222?useSSL=false");
				settings.put(Environment.USER, "kz");
				settings.put(Environment.PASS, "kzroot");
				settings.put(Environment.DIALECT, "org.hibernate.dialect.MySQL5Dialect");
				settings.put(Environment.SHOW_SQL, "true");
				settings.put(Environment.CURRENT_SESSION_CONTEXT_CLASS, "thread");
				// settings.put(Environment.HBM2DDL_AUTO, "create-drop");
				configuration.setProperties(settings);
				configuration.addAnnotatedClass(Actividades.class);
				configuration.addAnnotatedClass(ActividadFa.class);
				configuration.addAnnotatedClass(Estados.class);
				configuration.addAnnotatedClass(Faces.class);
				configuration.addAnnotatedClass(FechaEspeciales.class);
				configuration.addAnnotatedClass(Personas.class);
				configuration.addAnnotatedClass(TActividades.class);
				configuration.addAnnotatedClass(TipoUsuarios.class);
				configuration.addAnnotatedClass(Usuarios.class);
				ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
						.applySettings(configuration.getProperties()).build();
				sessionFactory = configuration.buildSessionFactory(serviceRegistry);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return sessionFactory;
	}
}