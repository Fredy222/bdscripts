package com.f.utils;

import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import com.f.dao.ActividaDAO;
import com.f.dao.ActividadFDAO;
import com.f.dao.EstadoDAO;
import com.f.dao.FacesDAO;
import com.f.dao.FechasEspecialesDAO;
import com.f.dao.PersonaDAO;
import com.f.dao.TActividadesDAO;
import com.f.dao.TipoUsuarioDAO;
import com.f.dao.UsuarioDAO;
import com.f.models.*;

@Configuration
@EnableWebMvc
@ComponentScan("com.f")
@EnableTransactionManagement(proxyTargetClass = true)
public class Config {

	@Bean
	InternalResourceViewResolver viewRes() {
		InternalResourceViewResolver r = new InternalResourceViewResolver();
		r.setPrefix("/view/");
		r.setSuffix(".jsp");
		return r;
	}

	@Bean
	public SessionFactory getConex() {
		return HibernateUtil.getSessionFactory();
	}
	
	@Bean
	public ActividaDAO actividad() {
		return new ActividaDAO();
	}
	@Bean
	public Dao<ActividadFa> actividadF() {
		return new ActividadFDAO();
	}
	@Bean
	public Dao<Estados> estado() {
		return new EstadoDAO();
	}
	@Bean
	public Dao<Faces> face() {
		return new FacesDAO();
	}
	@Bean
	public FechasEspecialesDAO fe() {
		return new FechasEspecialesDAO();
	}
	@Bean
	public PersonaDAO persona() {
		return new PersonaDAO();
	}
	@Bean
	public TActividadesDAO ta() {
		return new TActividadesDAO();
	}
	@Bean
	public Dao<TipoUsuarios> tipous() {
		return new TipoUsuarioDAO();
	}
	@Bean
	public UsuarioDAO usuario() {
		return new UsuarioDAO();
	}

}