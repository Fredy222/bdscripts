/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.f.controller;

import com.f.dao.FechasEspecialesDAO;
import com.f.models.FechaEspeciales;
import com.f.utils.Dao;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author fredy.romerousam
 */
@SuppressWarnings("unused")
@Controller
public class ManejoAsueto {

    ModelAndView mv;
    @Autowired
    private FechasEspecialesDAO fe;

    SimpleDateFormat formatter1 = new SimpleDateFormat("yy-MM-dd");

    private List<FechaEspeciales> lista() {
        fe = new FechasEspecialesDAO();
        List<FechaEspeciales> lista = new ArrayList<>();
        lista = fe.findAll();
        return lista;
    }

    @RequestMapping(value = "/fechar")
    public ModelAndView fechar() {
        mv = new ModelAndView();
        mv.addObject("listaf", lista());
        mv.setViewName("fechas");
        return mv;
    }

    @RequestMapping(value = "/gf", method = RequestMethod.POST)
    public ModelAndView gf(@RequestParam("fe") String fec) throws Exception {

        Date d = formatter1.parse(fec);
        System.out.println("Llego-----");
        fe = new FechasEspecialesDAO();
        FechaEspeciales fecha = new FechaEspeciales();
        fecha.setFecha(d);
        fecha.setEstado(true);
        fe.save(fecha);
        mv = new ModelAndView();
        mv.addObject("listaf", lista());
        mv.setViewName("fechas");
        return mv;

    }

    @RequestMapping(value = "/estado", method = RequestMethod.POST)
    public ModelAndView gf(@RequestParam("i") int i, @RequestParam("f") String f, @RequestParam("e") boolean e) throws Exception {
        FechaEspeciales fech = new FechaEspeciales();
        fech.setId(i);
        Date d = formatter1.parse(f);
        fech.setFecha(d);
        fech.setEstado(e);
        if (fech.getEstado() == true) {
            fech.setEstado(false);
        } else if (fech.getEstado() == false) {
            fech.setEstado(true);
        }
        fe.update(fech);
        mv = new ModelAndView();
        mv.addObject("listaf", lista());
        mv.setViewName("fechas");
        return mv;
    }

    @RequestMapping(value = "/acender")
    public void cargar() {

    }
}
