package com.f.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.f.dao.ActividaDAO;
import com.f.dao.ActividadFDAO;
import com.f.dao.FechasEspecialesDAO;
import com.f.dao.PersonaDAO;
import com.f.dao.TActividadesDAO;
import com.f.models.ActividadFa;
import com.f.models.Actividades;
import com.f.models.Estados;
import com.f.models.Faces;
import com.f.models.Personas;
import com.f.models.TActividades;
import com.f.models.Usuarios;
import com.f.utils.Dao;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@SuppressWarnings("unused")
@Controller
public class ManejoAgenda {

    ModelAndView mv;

    @Autowired
    private Dao<TActividades> ta;

    @Autowired
    private PersonaDAO persona;

    @Autowired
    private FechasEspecialesDAO fe;

    @Autowired
    private Dao<Actividades> actividad;

    @Autowired
    private Dao<ActividadFa> actividadF;

    public boolean ver(Date f) {
        int dias = 0;
        int horas = 0;
        int minutos = 0;
        try {
            Date fechaFinal = new Date();
            Date fechaInicial = f;
            int diferencia = (int) ((fechaFinal.getTime() - fechaInicial.getTime()) / 1000);
            if (diferencia > 86400) {
                dias = (int) Math.floor(diferencia / 86400);
                diferencia = diferencia - (dias * 86400);
            }
            System.out.println("DIAS---------- " + dias);
            if (dias != 0) {
                return false;
            } else {
                return true;
            }
        } catch (Exception e) {
            return false;
        }

    }

    @RequestMapping(value = "/agendar")
    public ModelAndView agendar() {
        mv = new ModelAndView();
        persona = new PersonaDAO();
        ta = new TActividadesDAO();
        try {
            List<TActividades> lista = new ArrayList<>();
            lista = ta.findAll();
            List<Personas> listaP = new ArrayList<>();
            listaP = persona.listaP();
            System.out.println("*-------------------* datos" + lista.size() + " " + listaP.size());
            mv.addObject("tipo", lista);
            mv.addObject("person", listaP);
            mv.setViewName("agenda");
            return mv;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    //carga de lista de actividadad;
    private List<Actividades> lista() {
        actividad = new ActividaDAO();
        List<Actividades> listAc = new ArrayList<>();
        listAc = actividad.findAll();
        return listAc;
    }

    private String caF(boolean f, Date d) throws Exception {
        fe = new FechasEspecialesDAO();
        SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
        System.out.println("respuesta si es true o false " + f);
        Date d3 = null;
        if (f == true) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(d);
            calendar.add(Calendar.DAY_OF_YEAR, 1);
            Date newF = calendar.getTime();
            String d2 = formatter1.format(newF);
            Date f1 = formatter1.parse(d2);
            System.out.println("datos de entrada de true " + d2 + " ----- " + f1);
            fe = new FechasEspecialesDAO();
            boolean vfe = fe.verificacion(d2);
            return (caF(vfe, f1));
        } else {
            d3 = d;
            System.out.println("d3 ----- " + d3.toString());
        }
        System.out.println("d3 02 ---------" + d3.toString());
        return formatter1.format(d3);
    }

    private String fechaM(String f) throws Exception {
        fe = new FechasEspecialesDAO();
        boolean vfe = fe.verificacion(f);
        String ca = "";
        if (vfe == false) {
            ca = f;
            System.out.println("datos de salida de fecha M if " + ca);
        } else if (vfe == true) {
            SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
            Date d = formatter1.parse(f);
            ca = caF(vfe, d);
            System.out.println("datos de salida de fecha M else -" + ca);
        }
        return ca;
    }

    @RequestMapping(value = "/guardarG", method = RequestMethod.POST)
    public ModelAndView guardarG(@RequestParam("ac") String ac, @RequestParam("des") String des,
            @RequestParam("tipo") int tipo, @RequestParam("fecha") String fecha, @RequestParam("perso") int perso, @RequestParam("u") int usua) throws Exception {
        String fehcaSa = fechaM(fecha);
        mv = new ModelAndView();
        SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");

        Date d = formatter1.parse(fehcaSa);
        boolean re = ver(d);
        if (re == true) {
            boolean vfe = fe.verificacion(fecha);
            actividad = new ActividaDAO();
            TActividades t = new TActividades();
            t.setId(tipo);
            Personas p = new Personas();
            p.setId(perso);
            Actividades act = new Actividades();
            act.setDescripcion(des);
            act.setFecha(formatter1.parse(fehcaSa));
            act.setNombre(ac);
            act.setPersonas(p);
            act.setTActividades(t);
            Usuarios u = new Usuarios();
            u.setId(usua);
            act.setUsuarios(u);
            actividad.save(act);
            //Actividad con Face y su estado
            actividadF=new ActividadFDAO();
            ActividadFa acf=new ActividadFa();
            acf.setActividades(act);
            Date da=new Date();
            acf.setFecha(da);
            Estados es=new Estados();
            es.setId(1);
            acf.setEstados(es);
            Faces fa=new Faces();
            fa.setId(1);
            acf.setFaces(fa);
            actividadF.save(acf);
            //--------------
            mv.setViewName("inicio");
            mv.addObject("msj", "");
            mv.addObject("lista", lista());
        } else if (re == false) {
            mv.setViewName("inicio");
            mv.addObject("lista", lista());
            mv.addObject("msj", "<div class='alert alert-danger' role='alert'>La Fecha No Puede Ser Anterior a la actual Verifique !!</div>");
        }
        return mv;
    }

    @RequestMapping(value = "/llegada")
    public ModelAndView ver() {
        actividad = new ActividaDAO();
        mv = new ModelAndView();
        List<Actividades> listAc = new ArrayList<>();
        listAc = actividad.findAll();
        System.out.println("Lista de actividad " + listAc.size());
        mv.setViewName("inicio");
        mv.addObject("lista", listAc);
        return mv;
    }

    @RequestMapping(value = "/edA", method = RequestMethod.GET)
    public ModelAndView editar(@RequestParam("m") int d) {
        mv = new ModelAndView();
        actividad = new ActividaDAO();
        List<TActividades> lista = new ArrayList<>();
        lista = ta.findAll();
        List<Personas> listaP = new ArrayList<>();
        listaP = persona.findAll();

        mv.addObject("tipo", lista);
        mv.addObject("person", listaP);
        Actividades a = new Actividades();
        a = actividad.readById(d);
        mv.setViewName("update");
        mv.addObject("a", a);
        return mv;
    }

    @RequestMapping(value = "/upA", method = RequestMethod.POST)
    public ModelAndView editarA(@RequestParam("ac") String ac, @RequestParam("des") String des,
            @RequestParam("tipo") int tipo, @RequestParam("fecha") String fecha, @RequestParam("perso") int perso, @RequestParam("id") int id) throws Exception {
        mv = new ModelAndView();
        String fehcaSa = fechaM(fecha);
        SimpleDateFormat formatter1 = new SimpleDateFormat("yy-MM-dd");
        Date d = formatter1.parse(fehcaSa);
        if (ver(d) == true) {
            actividad = new ActividaDAO();
            TActividades t = new TActividades();
            t.setId(tipo);
            Personas p = new Personas();
            p.setId(perso);
            Actividades act = new Actividades();
            act.setId(id);
            act.setDescripcion(des);
            act.setFecha(formatter1.parse(fehcaSa));
            act.setNombre(ac);
            act.setPersonas(p);
            act.setTActividades(t);
            Usuarios u = new Usuarios();
            u.setId(2);
            act.setUsuarios(u);
            actividad.update(act);
            mv.setViewName("inicio");
            mv.addObject("lista", lista());
            return mv;
        } else {
            mv.setViewName("inicio");
            mv.addObject("lista", lista());
            mv.addObject("msj", "<div class='alert alert-danger' role='alert'>La Fecha No Puede Ser Anterior a la Actual  Verifique !!</div>");
            return mv;

        }
    }

}
