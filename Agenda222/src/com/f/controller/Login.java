/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.f.controller;

import com.f.dao.ActividaDAO;
import com.f.dao.UsuarioDAO;
import com.f.models.Actividades;
import com.f.models.Usuarios;
import com.f.utils.Dao;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author fredy.romerousam
 */
@SuppressWarnings("unused")
@Controller
public class Login {

    ModelAndView mv;

    @Autowired
    private UsuarioDAO usuario;

    @Autowired
    private ActividaDAO actividad;

    Usuarios res;

    @RequestMapping(value = "/logear")
    public ModelAndView car() {
        mv = new ModelAndView();
        mv.setViewName("login");
        return mv;
    }

    @RequestMapping(value = "/verificar", method = RequestMethod.POST)
    public ModelAndView ver(@RequestParam("usu") String usu, @RequestParam("pa") String pa, HttpServletRequest request) {
        mv = new ModelAndView();
        usuario = new UsuarioDAO();
        res = new Usuarios();
        res = usuario.verificacion(usu, pa);
        try {

            if (res.getId() > 0) {
                int t = res.getTipoUsuarios().getId();
                if (t == 1 || t == 2) {
                    HttpSession session = request.getSession();
                    session.setAttribute("userLoggin", res);
                    //mv.setViewName("redirect:/inicio");
                    mv.setViewName("index02");
                } else if (t == 3) {
                    System.out.println("llego al 3");
                    List<Actividades> lis = lista(res.getPersonas().getId());
                    mv.addObject("lista", lis);
                    System.out.println("paso del 03");
                    HttpSession session = request.getSession();
                    session.setAttribute("userLoggin", res);
                    mv.setViewName("index03");
                }
            } else {
                mv.addObject("msj", "<div class='alert alert-danger' role='alert'>Verifique su Usuario y Contraseņa</div>");
                mv.setViewName("login");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mv;
    }

    private List<Actividades> lista(int i) {
        try {
            actividad = new ActividaDAO();
            List<Actividades> listAc = new ArrayList<>();
            listAc = actividad.verificacion(i);
            return listAc;
        } catch (Exception e) {
            e.printStackTrace();
            return null;

        }

    }

    // Metodo para cerrar session
    @RequestMapping(value = "/closelogin", method = RequestMethod.GET)
    public ModelAndView loginIn(HttpSession session) {
        session.removeAttribute("userLoggin");
        session.setAttribute("userLoggin", null);
        session.invalidate();
        res = new Usuarios();
        mv.addObject("userLoggin", null);
        mv.setViewName("redirect:/inicio");
        return mv;
    }
}
