package com.f.controller;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.ServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.f.dao.ActividaDAO;
import com.f.models.Actividades;
import com.f.reporte.Rep01;
import com.f.utils.Dao;

import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Controller
public class Reportes {
	ModelAndView mv = new ModelAndView();

	@Autowired
	ServletContext servletContext;

	@Autowired
	ServletResponse response;

	@Autowired
	private ActividaDAO actividad;

	@RequestMapping(value = "/newRep")
	public ModelAndView newRep() {
		mv.setViewName("repCa");
		return mv;
	}

	@RequestMapping(value = "/car1", method = RequestMethod.GET)
	public void repo01(@RequestParam("n") String n, @RequestParam("c") int c) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("na", n);
		String rep = "cA.jasper";
		actividad = new ActividaDAO();
		List<Rep01> lista = actividad.cargarA(c);
		JRBeanCollectionDataSource ds = new JRBeanCollectionDataSource(lista);
		String d = "reporte01.jasper";
		reporte(d, parameters, ds);

	}

	public void reporte(String d, Map<String, Object> d2, JRBeanCollectionDataSource d3) {
		try {
			File reportFile = new File(servletContext.getRealPath("/Reporte/" + d));

			byte[] bytes = JasperRunManager.runReportToPdf(reportFile.getPath(), d2, d3);
			response.setContentType("application/pdf");
			response.setContentLength(bytes.length);
			ServletOutputStream ouputStream = response.getOutputStream();
			ouputStream.write(bytes, 0, bytes.length);
			ouputStream.flush();
			ouputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
