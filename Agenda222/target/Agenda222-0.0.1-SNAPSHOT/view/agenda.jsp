<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page session="true"%>
<%@page import="com.f.models.Usuarios"%>
<%
	if (session.getAttribute("userLoggin") == null || session.getAttribute("userLoggin").equals("")) {
		response.sendRedirect("login");
	}
%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="pragma" content="no-cache">
        <title>Insert title here</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    </head>
    <body>
        <div class="container">
            <jsp:include page="menu.jsp" />
            <h1>Registro de Fecha</h1>
            <div class="row">
                <form method="POST" action="guardarG">
                    <input type="hidden" value="${userLoggin.id}" id="u" name="u">
                    <div class="form-group">
                        <label>Nombre de actividad:</label> <input type="text" id="ac" name="ac" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Descripcion:</label>
                        <textarea id="des" name="des" class="form-control form-control-lg" rows="4"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Tipo de Tarea:</label> <select id="tipo" name="tipo">
                            <c:forEach items="${tipo}" var="t">
                                <option value="${t.id}">${t.nombre}</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Fecha de Realizacion:</label>
                        <input type="date" class="form-control" id="fecha" name="fecha">
                    </div>
                    <div class="form-group">
                        <label>Persona:</label> <select id="perso" name="perso">
                            <c:forEach items="${person}" var="p">
                                <option value="${p.id}">${p.nombre}</option>
                            </c:forEach>
                        </select>
                    </div>
                    <button class="btn btn-success">GUARDAR</button>
                </form>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"></script>
        <script	src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
        <script	src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script></body>
</html>