<%-- 
    Document   : fechas
    Created on : 12-13-2019, 10:56:11 AM
    Author     : fredy.romerousam
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="windows-1252"%>
<%@page session="true"%>
<%@page import="com.f.models.Usuarios"%>
<%
    if (session.getAttribute("userLoggin") == null || session.getAttribute("userLoggin").equals("")) {
        response.sendRedirect("login");
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="pragma" content="no-cache">
        <title>JSP Page</title>
        <link rel="stylesheet"	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    </head>
    <body>
        <div class="container">
            <h1>Agenda F222</h1>
            <jsp:include page="menu.jsp" />
            <div class="row">
                <h4 class="text-primary">Ingreso de Fechas Especiales</h4>
            </div>
            <div class="row">
                <form action="gf" method="POST">
                    <div class="form-group">
                        <label>Fecha: </label>
                        <input type="date" class="form-control" id="fe" name="fe" />
                    </div>
                    <button class="btn btn-success">Guardar</button>
                </form>
            </div>
            <br>
            <div class="table-responsive-sm">
                <table class="table table-striped " style="text-align:center;width:auto;">
                    <thead class="thead-dark">
                        <tr>
                            <!--<button class="btn btn-light">Promover un a�o mas</button>-->
                            <th scope="col" colspan="4">Fechas de Asueto </th>
                        </tr>
                        <!--   <tr>
                                <th colspan="4"></th>
                           </tr>-->
                        <tr>
                            <th scope="col">Fecha</th>
                            <th scope="col">Estado</th>
                            <th scope="col">Descripcion</th>
                            <th scope="col">Opcion</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${listaf}" var="lf">
                            <tr>
                                <th scope="row">${lf.fecha}</th>                               
                                    <c:choose>
                                        <c:when test="${lf.estado==true}">
                                        <td class="bg-success">Activa</td>
                                    </c:when>    
                                    <c:otherwise>
                                        <td class="bg-danger">Inactiva</td>                
                                    </c:otherwise>   
                                </c:choose>  
                                <td>${lf.descripcion}</td>      
                                <td>
                                    <form action="estado" method="POST">
                                        <input type="hidden" id="i" name="i" value="${lf.id}">
                                        <input type="hidden" id="f" name="f" value="${lf.fecha}">
                                        <input type="hidden" id="e" name="e" value="${lf.estado}">
                                        <button class="btn btn-primary" >Cambiar Estado</button>
                                    </form>
                                </td>                      
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div> 
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    </body>
</html>
