<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page session="true"%>
<%@page import="com.f.models.Usuarios"%>
<%
	if (session.getAttribute("userLoggin") == null || session.getAttribute("userLoggin").equals("")) {
		response.sendRedirect("login");
	}
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
</head>
<body>
	<h1>Agenda F222</h1>
	<jsp:include page="menu.jsp" />
	<div class="container">
		<div class="row">
			<h3>Reportes de Usuario y Actividad</h3>
			<div class="col-md-12  border border-success">
				<br> <label>Tiempo de Fase "en Proceso" para ctividades
					asignadas</label>
				<a class="btn btn-success btn-lg" href="car1?c=${userLoggin.id}&n=${userLoggin.personas.nombre} ${userLoggin.personas.apellido}" target="_blank">REPORTE</a>
				<br>
				<hr size="2px" color="black" />
			</div>
		</div>
	</div>
	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"></script>
	<script
		src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</body>
</html>