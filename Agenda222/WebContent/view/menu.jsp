<%-- 
    Document   : menu
    Created on : 12-16-2019, 04:24:38 PM
    Author     : fredy.romerousam
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="windows-1252"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
        <title>JSP Page</title>
    </head>
    <body>   
        <div class="row" style="margin-left: 4%">
            <c:if test="${userLoggin.id ==2}">
                <a href="agendar" class="btn btn-success">Agregar Actividad</a>
                <a href="llegada" class="btn btn-primary">Actividades Registradas</a>
                <a href="newRep" class="btn btn-success">Reportes</a>
            </c:if>
            <br>
            <c:if test="${userLoggin.id ==1}">
                <a href="fechar" class="btn btn-primary">D�as De Asueto</a>
            </c:if>
                <a href="closelogin" class="btn btn-outline-danger">Cerrar Session</a>
        </div>
    </body>
</html>
