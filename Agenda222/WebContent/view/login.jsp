<%-- 
    Document   : login
    Created on : 12-14-2019, 11:25:38 AM
    Author     : fredy.romerousam
--%>

<%@page contentType="text/html" pageEncoding="windows-1252"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
        <title>JSP Page</title>
    </head>
    <body>
        <div class="container">
          
            <div class="row">
                <div class="col-md-12"></div>
                <div class="col-md-3"></div>
                <div class="col-md-6 border border-success" style="margin-top: 4%">
                    <form method="POST" action="verificar" style="padding: 4%;">
                        <div class="row text-center"><h1>Login</h1></div>
                        <div class="form-group">
                            <label>USUARIO</label>
                            <input id="usu" name="usu" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>CONTRASEŅA</label>
                            <input id="pa" name="pa" class="form-control" type="password">
                        </div>
                        <button class="btn btn-success">Verificar</button>
                    </form>
                    <div class="row">${msj}</div>
                </div>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"></script>
        <script	src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
        <script	src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    </body>
</html>
