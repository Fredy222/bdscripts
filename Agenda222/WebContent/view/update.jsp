<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page session="true"%>
<%@page import="com.f.models.Usuarios"%>
<%
    if (session.getAttribute("userLoggin") == null || session.getAttribute("userLoggin").equals("")) {
        response.sendRedirect("login");
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="pragma" content="no-cache">
        <title>Insert title here</title>
        <link rel="stylesheet"
              href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    </head>
    <body>
        <h1>AgendaF222</h1>
        <jsp:include page="menu.jsp" />
        <div class="container">
            <form method="POST" action="upA">
                <div class="form-group">
                    <label>Nombre de actividad:</label> <input type="text" id="ac" value="${a.nombre}"
                                                               name="ac" class="form-control">
                    <input type="hidden" id="id" value="${a.id}" name="id">
                </div>
                <div class="form-group">
                    <label>Descripcion:</label>
                    <textarea id="des" name="des" class="form-control form-control-lg"
                              rows="4">${a.descripcion}</textarea>
                </div>
                <div class="form-group">
                    <label>Tipo de Tarea:</label> <select id="tipo" name="tipo">
                        <c:forEach items="${tipo}" var="t">
                            <c:choose>
                                <c:when test="${a.TActividades.id==t.id}">
                                    <option value="${t.id}" selected="selected">${t.nombre}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${t.id}">${t.nombre}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </div>
                <div class="form-group">
                    <label>Fecha de Realizacion:</label>
                    <input type="date" class="form-control" id="fecha" name="fecha" value="${a.fecha}">
                </div>
                <div class="form-group">
                    <label>Persona:</label> <select id="perso" name="perso">
                        <c:forEach items="${person}" var="p">
                            <c:choose>
                                <c:when test="${a.personas.id==p.id}">
                                    <option value="${p.id}" selected="selected">${p.nombre}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${p.id}">${p.nombre}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>

                    </select>
                </div>
                <button class="btn btn-success">GUARDAR</button>
            </form>
        </div>
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"></script>
        <script
        src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
        <script
        src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    </body>
</html>