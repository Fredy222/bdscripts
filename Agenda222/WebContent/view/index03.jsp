<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@page session="true"%>
<%@page import="com.f.models.Usuarios"%>
<%
    if (session.getAttribute("userLoggin") == null || session.getAttribute("userLoggin").equals("")) {
        response.sendRedirect("login");
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="pragma" content="no-cache"> 
        <title>Insert title here</title>
        <link rel="stylesheet"
              href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    </head>
    <body>
        <h1>Agenda F222</h1>
        <div class="container">
               <a href="closelogin" class="btn btn-outline-danger">Cerrar Session</a>
            <div class="row">
                <c:forEach items="${lista}" var="l">
                    <div class="card" style="width: 18rem;">
                        <div class="card-body">
                            <h5 class="card-title">${l.nombre}</h5>
                            <h6 class="card-subtitle mb-2 text-muted">${l.fecha}</h6>
                            <p class="card-text">${l.descripcion}</p>
                            <a class="btn btn-primary" data-toggle="collapse"
                               href="#multiCollapseExample${l.id}" role="button"
                               aria-expanded="false" aria-controls="multiCollapseExample1">Detalles</a>
                            <div class="col">
                                <div class="collapse multi-collapse"
                                     id="multiCollapseExample${l.id}">
                                    <div class="card card-body">
                                        <p>
                                            <strong>Persona asignada:</strong> ${l.personas.nombre}
                                            ${l.personas.apellido}<br> <strong>Tipo de
                                                Tarea:</strong> ${l.TActividades.nombre}
                                        </p>
                                    </div>
                                </div>
                            </div>
                                        <br>
                            <a class="btn btn-success">Actualizar</a>
                        </div>
                    </div>
                </c:forEach>
            </div>
            <script     src="https://code.jquery.com/jquery-3.4.1.slim.min.js"></script>
            <script	src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
            <script	src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    </body>
</html>

