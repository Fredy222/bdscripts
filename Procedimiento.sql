DELIMITER //

CREATE PROCEDURE calculo (
    IN gerente INT,
    IN tarea INT
--    IN precio DECIMAL,
  --  IN comision DECIMAL
)
BEGIN

Select a.nombre,DATEDIFF((Select acf.fecha From actividades as a 
inner join usuarios as u on u.id=a.usuario
inner join personas as p on p.id=a.persona
inner join actividad_fa as acf on acf.actividad=a.id 
where acf.face=2 and u.id=gerente and acf.estado= 3 and a.id=tarea),acf.fecha)
as dias , p.nombre as persona
From actividades as a 
inner join usuarios as u on u.id=a.usuario
inner join personas as p on p.id=a.persona
inner join actividad_fa as acf on acf.actividad=a.id
where acf.face=2 and u.id=gerente and acf.estado=2 and a.id=tarea;
END//

DELIMITER ;