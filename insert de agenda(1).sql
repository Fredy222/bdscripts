insert into t_actividades(nombre) values('Urgente'),('Media');
insert into tipo_usuarios(nombre) values('Administrador'),('Gerente'),('Asistente');

insert into personas(nombre,apellido,telefono,correo) 
values('Fredy','Romero','000-000','f@gmail.com'),
('Alexi','Perez','1201-000','a@gmail.com'),
('Erick','Gomez','7541-4500','e@gmail.com'),
('Amanda','Quintanilla','000','q@gmail.com');


insert into usuarios(nombre,pass,persona,tipo,estado)
values('fredy','123',1,1,1),
('Alexi','123',2,2,1),
('Erick','123',3,3,1),
('AQ','123',4,3,1);
insert into fecha_especiales(fecha,estado,descripcion) values('2019/12/15',true,'Prueba 01');
insert into fecha_especiales(fecha,estado,descripcion) values('2019/12/16',false,'Prueba 02');

insert into faces(nombre) values('Inicio'),('Proceso'),('Finalizado');
insert into estados(nombre) values('Espera'),('Iniciada'),('Terminada');