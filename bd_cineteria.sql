drop database if exists bd_cineteria;
create database bd_cineteria;
use bd_cineteria;

create table clasificacion_pelicula(
id_clasificacion int not null primary key auto_increment,
clasificacion varchar(150),
detalle varchar(150)
);

create table tipo_tecnologia(
id_tipo int not null primary key auto_increment,
nombre varchar(15)
);
create table peliculas(
id_pelicula int not null primary key auto_increment,
nombre varchar(150),
duracion varchar(150),
id_tipo int,
id_clasificacion int,
director varchar(150),
detalle varchar(150),
image longblob null default null,
foreign key(id_tipo) references tipo_tecnologia(id_tipo),
foreign key(id_clasificacion) references clasificacion_pelicula(id_clasificacion)
);

create table salas(
id_sala int not null primary key auto_increment,
nombre varchar(150),
id_tipo int,
num_asientos int,
estado enum('Activo','Inactivo'),
foreign key(id_tipo) references tipo_tecnologia(id_tipo) on update cascade on delete cascade 
);

create table asientos(
id_asiento int not null primary key auto_increment,
nombre_asiento int,
id_sala int,
estado enum('Reservado','Disponible'),
foreign key(id_sala) references salas(id_sala)
);

create table horario(
id_horario int not null primary key auto_increment,
fecha_hora_emision datetime,
id_sala int,
id_pelicula int,
foreign key(id_sala) references salas(id_sala),
foreign key(id_pelicula) references peliculas(id_pelicula)
);

create table tipo_turno(
id_tipo_turno int not null primary key auto_increment,
turno enum('matutino', 'vespertino', 'extra','por hora'),
detalle varchar(150)
);


create table rol(
id_rol int not null primary key auto_increment,
nombre varchar(150)
);
create table persona(
id_persona int not null primary key auto_increment,
nombre varchar(150),
apellido varchar(150),
dui varchar(150),
fecha_nacimiento date,
direccion varchar(150),
telefono varchar(150)
);




create table turno_empleado(
id_turno int not null primary key auto_increment,
id_tipo_turno int,
id_empleado int,
foreign key(id_tipo_turno) references tipo_turno(id_tipo_turno)
);

create table empleado(
id_empleado int not null primary key auto_increment,
id_persona int,
id_rol int,
id_tipo_turno int,
foreign key(id_persona) references persona(id_persona) on update cascade on delete cascade ,
foreign key(id_rol) references rol(id_rol) on update cascade on delete cascade ,
foreign key(id_tipo_turno) references tipo_turno (id_tipo_turno) on update cascade on delete cascade 
);




create table tipo_boleto(
id_tipo_boleto int not null primary key auto_increment,
tipo varchar(150),
precio decimal (7,2)
);


create table factura(
id_factura int not null primary key auto_increment,
fecha date,
id_pelicula int,
total_pagar decimal (7,2)
);

create table boleto(
id_boleto int not null primary key auto_increment,
id_tipo_boleto int,
id_sala int,
id_factura int,
foreign key(id_tipo_boleto) references tipo_boleto(id_tipo_boleto),
foreign key(id_sala) references salas(id_sala),
foreign key(id_factura) references factura(id_factura)
);

create table detalle_factura(
id_detalle_factura int not null primary key auto_increment,
id_tipo_boleto int,
id_factura int,
cantidad int,
foreign key(id_tipo_boleto) references tipo_boleto(id_tipo_boleto),
foreign key(id_factura) references factura(id_factura)
);

create table inf_cine(
id_cine int not null primary key auto_increment,
nombre varchar(150),
ubicacion varchar(150),
telefono varchar(150),
correo varchar(150)
);

create table usuario(
id_usuario int not null primary key auto_increment,
id_empleado int,
id_rol int,
usuario varchar(150),
pass varchar(100),
foreign key(id_empleado) references empleado(id_empleado),
foreign key(id_rol) references rol(id_rol)
);




insert into tipo_tecnologia values(0,'2d');
insert into tipo_tecnologia values(0,'3d');
insert into tipo_tecnologia values(0,'normal');

insert into clasificacion_pelicula values(0,'A', 'para todo publico');
insert into clasificacion_pelicula values(0,'B', 'para Mayores de 12 años');
insert into clasificacion_pelicula values(0,'C', 'para todo publico');
insert into clasificacion_pelicula values(0,'D', 'para Mayores de 12 años');
insert into clasificacion_pelicula values(0,'E', 'para todo publico');


insert into tipo_boleto values(0,'niños', 4.00);
insert into tipo_boleto values(0,'adultos', 4.50);
insert into tipo_boleto values(0,'tercera edad', 3.50);

insert into rol values(0, 'Empleado');
insert into rol values(0, 'Administrador');

insert into persona values(0,'admin','admin','dui','2019-05-05','direccion','telefono');
insert into tipo_turno values(0,'extra','detalle');
insert into tipo_turno values(0,'matutino','detalle');
insert into tipo_turno values(0,'vespertino','detalle');
insert into empleado values (0,1,2,1);
insert into usuario values (0,1,2,'admin','21232f297a57a5a743894a0e4a801fc3');

insert into persona values(0,'empleado','empleado','dui','2019-05-05','direccion','telefono');
insert into empleado values (0,2,1,2);
insert into usuario values (0,1,1,'empleado','088ef99bff55c67dc863f83980a66a9b');
insert into inf_cine values(0, 'Cinetería', 'fwesf', 'fdsa', 'fsed' )





