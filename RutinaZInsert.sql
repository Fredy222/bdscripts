insert into cliente(nombre,apellido,direccion,fecha_nacimiento,telefono,email)
values('Fredy','Romero','Guazapa San Salvador','2000-02-02','12102','f@f.com'),
('Alexi','Perez','Guazapa San Salvador','2000-03-02','122','ap@f.com');


DELIMITER $$
CREATE PROCEDURE `sp_insercion_factura` (
in clien int,
in fech date,
in pag int)
BEGIN
insert into factura(id_cliente,fecha,num_pago) values(clien,fech,pag);
END