create database serviciosV;
use serviciosV;
create table viviendas(
id int not null primary key auto_increment,
c_habit int,
c_banios int,
colonia varchar(45),
departamento varchar(45),
municipio varchar(45),
precio decimal(5,2),
tamanio decimal(5,3),
categoria int,
negociable varchar(34),
estado boolean
)engine InnoDB;