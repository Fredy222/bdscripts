create database rutinaz;
use rutinaz;
create table modo_pago(
num_pago int not null primary key auto_increment,
nombre varchar(25),
otros_detalles text
)engine InnoDB ;
create table categoria(
id_categoria int not null primary key auto_increment,
nombre varchar(45),
descripcion text
)engine InnoDB;
create table producto(
id_producto int not null primary key auto_increment,
nombre varchar(50),
precio double(3,2),
stock int,
id_categoria int,
foreign key(id_categoria) references categoria(id_categoria)
)engine InnoDB;
create table cliente(
id_cliente int not null primary key auto_increment,
nombre varchar(25),
apellido varchar(25),
direccion text,
fecha_nacimiento date,
telefono varchar(15),
email varchar(45)
)engine InnoDB;
create table factura(
num_factura int not null primary key auto_increment,
fecha date,
id_cliente int,
num_pago int,
foreign key(id_cliente) references cliente(id_cliente),
foreign key(num_pago) references modo_pago(num_pago)
)engine InnoDB;
create table detalle(
num_detalle int not null primary key auto_increment,
id_factura int,
id_producto int,
cantidad int,
precio double(4,2),
foreign key(id_factura) references factura(num_factura),
foreign key(id_producto) references producto(id_producto)
)engine InnoDB;

